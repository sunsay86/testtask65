//
//  ViewController.swift
//  testTask4ScrolView
//
//  Created by Александр Волков on 15.02.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//
/* 4. Напишите код, реализующий закругление углов и тень для UIView (параметры тени любые). Разместите 500 таких view друг под другом в UIScrollView. Размеры view 50 на 50
// 5. Напишите код, поворачиващий UIView в 3D пространстве на 30 градусов по осям X, Y, Z

*/
import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    
    var newView =  UIView()
    
    
    var currentAnimation = 0
    
    var transform = CATransform3DIdentity
    
    
    private func addViews(numberOfViews: Int) {
        for i in 0 ..< numberOfViews {
            
            let newView =  UIView(frame: CGRect(x: 0, y: CGFloat(i) * 50, width: 50, height: 50)) //
            
            
            
            newView.layer.backgroundColor = UIColor.orange.cgColor
            newView.layer.cornerRadius = 15
            
            newView.layer.shadowRadius = 20.0
            newView.layer.shadowOpacity = 4
            newView.layer.shadowColor = UIColor.orange.cgColor
           
            scrollView.addSubview(newView)  
            
            
            if (i == 0) { self.newView = newView}
        }
        
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width,
                                        height: scrollView.frame.size.height * CGFloat(numberOfViews))
        
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            
            scrollView.delegate = self
            
            addViews(numberOfViews: 500) // если установлен srollView добавляем 500 view
        }
    }
     
    
    @IBAction func tapped(_ sender: Any) { // поворт на 30 градусов на каждый тап
        
        let angle: CGFloat = .pi/6
        
        switch currentAnimation {
            
        case 0: self.newView.layer.transform = CATransform3DRotate(transform, angle, 0, 0, 1)
        case 1: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(2), 0, 0, 1)
        case 2: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(3), 0, 0, 1)
        case 3: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(4), 0, 0, 1)
        case 4: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(5), 0, 0, 1)
        case 5: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(6), 0, 0, 1)
        case 6: self.newView.layer.transform = CATransform3DRotate(transform, angle, 0, 1, 0)
        case 7: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(2), 0, 1, 0)
        case 8: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(3), 0, 1, 0)
        case 9: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(4), 0, 1, 0)
        case 10: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(5), 0, 1, 0)
        case 11: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(6), 0, 1, 0)
        case 12: self.newView.layer.transform = CATransform3DRotate(transform, angle, 1, 0, 0)
        case 13: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(2), 1, 0, 0)
        case 14: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(3), 1, 0, 0)
        case 15: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(4), 1, 0, 0)
        case 16: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(5), 1, 0, 0)
        case 17: self.newView.layer.transform = CATransform3DRotate(transform, angle * CGFloat(6), 1, 0, 0)
        default:break
        }
        
        currentAnimation += 1
        if currentAnimation > 17 {
            currentAnimation = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    
}









    
