//
//  View.swift
//  testTask4ScrolView
//
//  Created by Александр Волков on 15.02.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//
/*
import Foundation
import UIKit

 class NewView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        self.backgroundColor = UIColor.orange
        self.bounds = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0
        
        self.layer.cornerRadius = 10.0
        
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.6
        
        self.layer.shadowColor = UIColor.black.cgColor
        
        self.layer.shadowOffset = CGSize.zero
        
        
        self.layer.masksToBounds = true
        
        
    }
 
}
 */
