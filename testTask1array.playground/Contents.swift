//: Playground - noun: a place where people can play

import Foundation

/*
 Напишите код, принимающий на вход массив неуникальных строк и возвращающий массив строк, замеченных в неуникальности.
 Пример:
 Входной массив: @[@"str1", @"str2", @"str1", @"str3"];
 Результат: @[@"str1"]
 */


var inputArray = ["str1", "str2", "str1", "str3", "str1", "str1", ""]

func deleteRepeatingMember(array: [String])->[String] {
    
    let sortedArray = array.sorted() // после сортировки все одинаковые строки будут рядом в массиве
    var outputArray = [String]()
    
    
    var str: String?
    
    for string in sortedArray {         // достаточно пройтись в цикле по массиву,
                                        // если член массива из текущей  итерации совпадает с членом массива из предыдущей,
                                        // добавляем его в выходной массив
        
        
        if string == str {
            
            outputArray.append(string)
        }
        
        str = string
        
    }
    return outputArray
}

deleteRepeatingMember(array: inputArray)







