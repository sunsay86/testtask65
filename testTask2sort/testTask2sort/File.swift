//
//  File.swift
//  testTask2sort
//
//  Created by Александр Волков on 01.03.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
class User {
    var firstName: String
    var lastName: String
    var age: Int
    
    init(firstName: String lastName: String, age: Int) {
        self.firstName = firstName 
        self.lastName = lastName
        self.age = age
    }
    
}


var AlexVolkov30 = User(firstName: "Alex", lastName: "Volkov", age: 30)
