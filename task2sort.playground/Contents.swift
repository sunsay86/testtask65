//: Playground - noun: a place where people can play
/*2.  Напишите код, который сортирует массив объектов по имени и фамилли, исключает из массива все объекты младше 18 лет.
 Объект:
 @inteface User : NSObject
 @property (nonatomic) NSString *firstName;
 @property (nonatomic) NSString *lastName;
 @property (nonatomic) NSNumber *age;
 @end*/
import Foundation


class User {
    var firstName: String
    var lastName: String
    var age: Int
    
    init(firstName: String, lastName: String, age: Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
    }
    
}


// создаем экземпляры класса User и добавляем их в массив
let alexVolkov30 = User(firstName: "Alex", lastName: "Volkov", age: 30)
let romanIvanov25 = User(firstName: "Roman", lastName: "Ivanov", age: 25)
let sonyaMakarova16 = User(firstName: "Sonya", lastName: "Makarova", age: 16)
let alexVasin20 = User(firstName: "Alex", lastName: "Vasin", age: 20)

var arrayOfObjects = [User]()

arrayOfObjects.append(alexVolkov30)
arrayOfObjects.append(romanIvanov25)
arrayOfObjects.append(sonyaMakarova16)
arrayOfObjects.append(alexVasin20)



arrayOfObjects

let sortedArray = arrayOfObjects.filter({ $0.age > 18 }).sorted {
        ($0.firstName == $1.firstName) ?
        ($0.lastName < $1.lastName) :
        ($0.firstName < $1.firstName)
}


sortedArray



