//: Playground - noun: a place where people can play

import UIKit
/*
   Напишите код, проверяющий, соответствует ли логин следующим правилам: логин может быть как email-ом так и обычной строкой. Минимальная длина логина - 3 символа, максимальная - 32. Логин может состоять из латинских букв, цифр, минуса и точки. Логин не может начинаться на цифру, точку, минус.
*/





let loginTextField = "log-in@icloud.com"


func isValid(login: String) -> Bool {
    let regex =  "[a-zA-Z][-0-9a-zA-Z@.]{2,31}$"
    let test = NSPredicate(format: "SELF MATCHES %@", regex)
    

    let result = test.evaluate(with: login)     
    return result
}


isValid(login: loginTextField)

