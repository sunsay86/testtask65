//
//  TableViewCell.swift
//  imageTableView
//
//  Created by Александр Волков on 15.02.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//
/*
 4. Напишите метод загрузки картинки в ячейку таблицы
 
 - (void)downloadImageFromURL:(NSURL )URL intoCell:(UITableViewCell )cell
 */

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imageForCell: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: Publi API
    
    var imageUrl: NSURL? {
        
        didSet {
            downloadImageFromURL()
        }
    }
    
    
    private func downloadImageFromURL()
    {
        
        print("1")
        if let url = imageUrl // если получен url запускаем spinner и создаем параллельную очередь global
        {
            
            spinner?.startAnimating()
            
            DispatchQueue.global(qos: .userInitiated).async //параллельная очередь - в ней пыаемс получить данные data по url
                {
                    
                    // получение данных из сети блокирует поток, на котором выполняется, поэтому этот процесс выполняем не в основной очереди, чтобы не блокировать интерфейс пользователя
                    let contentsOfURL = NSData(contentsOf: url as URL) // получаем данные из сети
                    
                                                                        // результат передаем в main Q асинхронно - пока не загрузится в бэкграунде картинка -  UIImageView не установится на экране
                                                                        //
                    DispatchQueue.main.async // main queue
                        { //[weak self] in
                            
                            
                            if url == self.imageUrl // если наш mvc в поле зрения польователя, если VС умер(все его свойства в том числе и imageUrl = nil) вышел пользак, то не будем ничего отображать
                            {
                                
                                if let imageData = contentsOfURL {  // если данные получены
                                    
                                    
                                    self.imageForCell?.image = UIImage(data: imageData as Data ) // форматируем изображение
                                                                                                // мы можем не использовать weak self , так как уже проверили что VC жив
                                }
                                
                                
                                self.spinner?.stopAnimating()
                            }
                            
                        }
                    
                }
           
        }
        
    }
}


